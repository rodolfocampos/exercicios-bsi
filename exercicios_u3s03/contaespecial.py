from conta import Conta


class ContaEspecial(Conta):

    def __init__(self, numero, titular, limite):
        super().__init__(numero, titular)
        self.__limite = limite
        self.__saldo = 0.0

    def __pode_sacar(self, valor):
        if self.valor_valido(valor):
            valor_disponivel = self.__saldo + self.__limite
            return valor <= valor_disponivel

    def saca(self, valor):
        if self.__pode_sacar(valor):
            self.__saldo -= valor
        else:
            print("O valor {} passou o limite".format(valor))
            print("Seu saldo atual é de {}, limite {}".format(self.__saldo, self.__limite))

    @property
    def limite(self):
        return self.__limite

    @limite.setter
    def limite(self, limite):
        self.__limite = limite
