class Conta:

    def __init__(self, numero, titular):
        self.__numero = numero
        self.__titular = titular
        self.__saldo = 0.0

    def extrato(self):
        print("Saldo de {} do titular {}".format(self.__saldo, self.__titular))

    def deposita(self, valor):
        if self.valor_valido(valor):
            self.__saldo += valor

    def __pode_sacar(self, valor):
        if self.valor_valido(valor):
            valor_disponivel_a_sacar = self.__saldo
            return valor <= valor_disponivel_a_sacar

    def valor_valido(self, valor):
        if valor > 0:
            return True
        else:
            print("O valor {} não é válido para transação".format(valor))
            return False

    def saca(self, valor):
        if self.__pode_sacar(valor):
            self.__saldo -= valor
        else:
            print("O valor {} passou o limite".format(valor))
            print("Seu saldo atual é de {}".format(self.__saldo))

    def transfere(self, valor, destino):
        self.saca(valor)
        destino.deposita(valor)

    @property
    def saldo(self):
        return self.__saldo

    @property
    def titular(self):
        return self.__titular
