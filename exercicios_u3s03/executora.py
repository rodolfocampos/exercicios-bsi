from conta import Conta
from contaespecial import ContaEspecial


def cadastra_conta():
    print("\nIniciando o cadastro de conta\n")
    numero = int(input("Digite o numero da conta: "))
    titular = input("Digite o nome do titular: ")

    while True:
        tipo_conta = int(input("\nTipo da conta: 1 - Conta simples | 2 - Especial\n"))
        if tipo_conta == 1:
            conta = Conta(numero, titular)
            break
        elif tipo_conta == 2:
            limite = float(input("Digite o limite da conta: "))
            conta = ContaEspecial(numero, titular, limite)
            break

    return conta


conta1 = cadastra_conta()
conta2 = cadastra_conta()

conta1.deposita(100)
conta2.deposita(100)

conta1.saca(150)
conta2.saca(150)

conta1.saca(80)
conta2.saca(20)
