$(document).on('click', '#cargo', function () {
    var valor = $(this).val();
    if (valor == 1) {
        $('#opcao2').hide();
        $('#dependentes').prop("disabled", true);

        $('#opcao1').show();
        $('#titulacao').prop("disabled", false);

        $('#botao-enviar').prop("disabled", false);

    } else if (valor == 2) {
        $('#opcao1').hide();
        $('#titulacao').prop("disabled", true);

        $('#opcao2').show();
        $('#dependentes').prop("disabled", false);

        $('#botao-enviar').prop("disabled", false);
    } else {
        $('#opcao2').hide();
        $('#dependentes').prop("disabled", true);
        $('#opcao1').hide();
        $('#titulacao').prop("disabled", true);

        $('#botao-enviar').prop("disabled", true);
    }
});

$(document).on('submit', '#form-funcionario', function (event) {
    event.preventDefault();

    $.ajax({
        type: "POST",
        url: this.action,
        data: $(this).serialize(), // serializes the form's elements.
        success: function (data) {
            $('#principal').html(data);
        }
    });
});

$(document).on('submit', '#form-area', function (event) {
    event.preventDefault();

    $.ajax({
        type: "POST",
        url: this.action,
        data: $(this).serialize(), // serializes the form's elements.
        success: function (data) {
            $('#area').val(data);
        }
    });
});