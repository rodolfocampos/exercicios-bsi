package br.com.rdo.exerciciosoo.model.exercicio1;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Administrativo extends Funcionario {

    private int dependentes;
    private BigDecimal contribuicao;


    public Administrativo(String nome, String cpf, String salario, int dependentes) {
        this.setNome(nome);
        this.setCpf(cpf);
        this.setSalario(new BigDecimal(salario));
        this.dependentes = dependentes;
        this.setContribuicao();
    }

    public BigDecimal getContribuicao() {
        return contribuicao.setScale(2, RoundingMode.HALF_UP);
    }

    public void setContribuicao() {
        this.contribuicao = getContribuicaoSindical();
    }

    public int getDependentes() {
        return dependentes;
    }

    public void setDependentes(int dependentes) {
        this.dependentes = dependentes;
    }

    public BigDecimal getContribuicaoSindical() {
        if (this.getSalario() != null) {
            BigDecimal contribuicao = this.getSalario().multiply(new BigDecimal("0.01"));
            return contribuicao;
        }
        return new BigDecimal("0.0");
    }

}
