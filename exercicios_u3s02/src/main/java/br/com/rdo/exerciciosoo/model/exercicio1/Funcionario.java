package br.com.rdo.exerciciosoo.model.exercicio1;

import java.math.BigDecimal;
import java.math.RoundingMode;

public abstract class Funcionario {

	private String nome;
	private String cpf;
	private BigDecimal salario;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public BigDecimal getSalario() {
		if (salario != null)
			return salario.setScale(2, RoundingMode.HALF_UP);
		return salario;
	}

	public void setSalario(BigDecimal salario) {
		this.salario = salario;
	}

}
