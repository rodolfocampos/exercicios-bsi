package br.com.rdo.exerciciosoo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExerciciosOoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExerciciosOoApplication.class, args);
	}

}
