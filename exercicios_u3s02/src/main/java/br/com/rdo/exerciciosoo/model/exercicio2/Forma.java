package br.com.rdo.exerciciosoo.model.exercicio2;

import java.math.BigDecimal;
import java.math.RoundingMode;

abstract class Forma {

    private BigDecimal base;
    private BigDecimal altura;
    private BigDecimal area;

    public BigDecimal getArea() {
        if (area != null) return area.setScale(2, RoundingMode.HALF_UP);
        return new BigDecimal("0.0");
    }

    public void setArea(BigDecimal area) {
        this.area = calcularArea();
    }

    public BigDecimal getBase() {
        return base;
    }

    public void setBase(BigDecimal base) {
        this.base = base;
    }

    public BigDecimal getAltura() {
        return altura;
    }

    public void setAltura(BigDecimal altura) {
        this.altura = altura;
    }

    public abstract BigDecimal calcularArea();
}
