package br.com.rdo.exerciciosoo.model.exercicio2;

import java.math.BigDecimal;

public class Retangulo extends Forma {

    public Retangulo(String base, String altura) {
        this.setBase(new BigDecimal(base));
        this.setAltura(new BigDecimal(altura));
        this.setArea(calcularArea());
    }

    @Override
    public BigDecimal calcularArea() {
        return this.getBase().multiply(this.getAltura());
    }
}
