package br.com.rdo.exerciciosoo.controller;

import br.com.rdo.exerciciosoo.model.exercicio1.Administrativo;
import br.com.rdo.exerciciosoo.model.exercicio1.Professor;
import br.com.rdo.exerciciosoo.model.exercicio2.Retangulo;
import br.com.rdo.exerciciosoo.model.exercicio2.TrianguloRetangulo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

    @RequestMapping("/")
    public String visualizarIndex() {

        return "index.html";
    }

    @RequestMapping("/exercicio1")
    public String visualizarExercicio1() {

        return "exercicio1.html";
    }

    @PostMapping("/enviar")
    public String processarFuncionario(@RequestParam(value = "nome") String nome, @RequestParam(value = "cpf") String cpf,
                                       @RequestParam(value = "salario") String salario, @RequestParam(value = "cargo") Integer cargo,
                                       @RequestParam(value = "dependentes", required = false) Integer dependentes,
                                       @RequestParam(value = "titulacao", required = false) String titulacao, Model model) {

        if (cargo == 1) {
            Professor professor = new Professor(nome, cpf, salario, titulacao);
            model.addAttribute("professor", professor);
        } else if (cargo == 2) {
            Administrativo administrativo = new Administrativo(nome, cpf, salario, dependentes);
            model.addAttribute("administrativo", administrativo);
        }

        return "resposta1.html";
    }

    @RequestMapping("/exercicio2")
    public String visualizarExercicio2() {

        return "exercicio2.html";
    }

    @PostMapping("/area")
    @ResponseBody
    public String calcularArea(@RequestParam(value = "base") String base, @RequestParam(value = "altura") String altura,
                               @RequestParam(value = "tipo") Integer tipo) {

        String resultado = "0.00";

        if (tipo == 2) {
            Retangulo retangulo = new Retangulo(base, altura);
            resultado = retangulo.getArea().toString();
        } else if (tipo == 1) {
            TrianguloRetangulo trianguloRetangulo = new TrianguloRetangulo(base, altura);
            resultado = trianguloRetangulo.getArea().toString();
        }

        return resultado;
    }

}
