package br.com.rdo.exerciciosoo.model.exercicio1;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Professor extends Funcionario {

    private String titulacao;
    private BigDecimal imposto;

    public String getTitulacao() {
        return titulacao;
    }

    public void setTitulacao(String titulacao) {
        this.titulacao = titulacao;
    }

    public BigDecimal getImposto() {
        return imposto.setScale(2, RoundingMode.HALF_UP);
    }

    public void setImposto() {
        this.imposto = getImpostoRenda();
    }

    public BigDecimal getImpostoRenda() {
        if (this.getSalario() != null) {
            BigDecimal imposto = this.getSalario().multiply(new BigDecimal("0.23"));
            return imposto;
        }
        return new BigDecimal("0.0");
    }

    public Professor(String nome, String cpf, String salario, String titulacao) {
        this.setNome(nome);
        this.setCpf(cpf);
        this.setSalario(new BigDecimal(salario));
        this.titulacao = titulacao;
        this.setImposto();
    }
}
